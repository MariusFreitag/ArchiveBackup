﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Globalization;
using System.Drawing;
using System.Windows.Media;

namespace ArchiveBackup.Converter
{
    class NaturalNumberOrZeroConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var number = int.Parse((string)value);

                if (number < 0)
                {
                    throw new Exception();
                }

                return number;
            }
            catch
            {
                return 0;
            }
        }
    }
}
