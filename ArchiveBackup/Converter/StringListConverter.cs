﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using System.Globalization;

namespace ArchiveBackup.Converter
{
    class StringListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Join(Environment.NewLine, ((List<string>)value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new List<string>(((string)value).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}
