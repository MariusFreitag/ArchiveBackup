﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Threading.Tasks;
using System.Globalization;
using System.Drawing;
using System.Windows.Media;
using ArchiveBackup.Helper;

namespace ArchiveBackup.Converter
{
    class PathToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new ImageBrush(ImageHelper.BitmapToImageSource(ImageHelper.ReadBitmap(value.ToString())));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
