﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using Path = System.IO.Path;
using File = System.IO.File;
using ArchiveBackup.Configuration;
using ArchiveBackup.Helper;
using System.Windows.Controls;
using IWshRuntimeLibrary;
using System.Linq;
using ArchiveBackup.Worker;
using System.IO;

namespace ArchiveBackup
{
    public partial class MainWindow : Window
    {
        private List<BackupRoutineControl> routineControls = new List<BackupRoutineControl>();
        private readonly string _configPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "ArchiveBackup.config");
        private Configuration.Configuration _config;
        private List<IWorker> _runningWorkerInstances = new List<IWorker>();

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                _config = JsonConvert.DeserializeObject<Configuration.Configuration>(File.ReadAllText(_configPath));
            }
            catch
            {
            }

            _config = _config ?? new Configuration.Configuration();

            ReloadInnerGrid();
        }

        public new void Show()
        {
            // Check for instant start
            if (Environment.GetCommandLineArgs().Length >= 2
                && Environment.GetCommandLineArgs()[1] == "/start")
            {
                BackupRoutine routine = _config.Routines.Where((r) => r.Name == Environment.GetCommandLineArgs()[2]).FirstOrDefault();

                if (routine != null)
                {
                    InstantStartBackup(routine);
                    return;
                }
            }

            base.Show();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            AddRoutine();
        }

        private void Credits_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://mariusfreitag.de");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _runningWorkerInstances.ForEach((i) => i.Abort());
            _config.Routines.ForEach(ArchiveFile.CleanupArchiveFiles);
        }

        private void WriteConfiguration()
        {
            DirectoryHelper.CreateAsDir(Path.GetDirectoryName(_configPath));

            if (!File.Exists(_configPath) || File.ReadAllText(_configPath) != JsonConvert.SerializeObject(_config, Formatting.Indented))
            {
                File.WriteAllText(_configPath, JsonConvert.SerializeObject(_config, Formatting.Indented));
            }
        }

        public void ReloadInnerGrid()
        {
            _config.Routines.ForEach(ArchiveFile.CleanupArchiveFiles);

            _config.Routines.Sort(new Comparison<BackupRoutine>((r1, r2) => r1.Name.CompareTo(r2.Name)));

            WriteConfiguration();

            InnerGrid.Children.Clear();

            // Display message when no routines are configured
            if (_config.Routines.Count <= 0)
            {
                InnerGrid.Children.Add(new Label()
                {
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(0, 10, 0, 0),
                    Content = "No routines configured."
                });
            }

            // Display all routines
            foreach (var routine in _config.Routines)
            {
                BackupRoutineControl control = new BackupRoutineControl(
                    routine,
                    onBackup: (update) => StartBackup(routine, update),
                    onRestore: (update) => StartRestore(routine, update),
                    onOpen: () => OpenDestinations(routine),
                    onSettings: () => OpenSettings(routine),
                    onCreateShortcut: () => CreateAndShowShortcut(routine)
                );

                control.Width = InnerGrid.Width - 50;
                control.SetStatus(GenerateRoutineStatus(routine), false);

                InnerGrid.Children.Add(control);

                // Add Space between children
                InnerGrid.Children.Add(new Separator() { Opacity = 0, Height = 10 });
            }
        }

        private void AddRoutine()
        {
            BackupRoutine newRoutine = new BackupRoutine("Routine");

            _config.Routines.Add(newRoutine);

            new RoutineSettingsWindow(newRoutine, delegate
            {
                _config.Routines.Remove(newRoutine);
                WriteConfiguration();
                ReloadInnerGrid();

            }).ShowDialog();

            WriteConfiguration();
            ReloadInnerGrid();
        }

        private void InstantStartBackup(BackupRoutine routine)
        {
            IWorker backupWorker = new BackupWorker(routine);
            _runningWorkerInstances.Add(backupWorker);
            ProgressWindow progressWindow = new ProgressWindow(routine.Name, backupWorker.Abort);
            progressWindow.Show();

            backupWorker.Work(
                onProgress: (p, m) =>
                {
                    if (p >= 0)
                        progressWindow.SetProgress(backupWorker.GetItemCount(), p, m);
                    progressWindow.SetIndeterminate(p < 0, m);
                },
                onError: (e) => progressWindow.ShowMessageBox($"Backup of {routine.Name} failed ({e.Message})"),
                onSuccess: () =>
                {
                    progressWindow.Dispatcher.Invoke(progressWindow.Hide);
                    progressWindow.ShowMessageBox($"Backup of {routine.Name} complete");
                },
                onFinally: () => Environment.Exit(0)
            );
        }

        private void StartBackup(BackupRoutine routine, Action<string, bool> update)
        {
            IWorker backupWorker = new BackupWorker(routine);
            _runningWorkerInstances.Add(backupWorker);

            backupWorker.Work(
                onProgress: (p, m) => update(p < 0 ? m : "Backupping: " + (int)Math.Round((double)p / backupWorker.GetItemCount() * 100) + "%", true),
                onError: (e) => MessageBox.Show($"Backup of {routine.Name} failed ({e.Message})"),
                onSuccess: () => MessageBox.Show($"Backup of {routine.Name} succeeded"),
                onFinally: () =>
                {
                    _runningWorkerInstances.Remove(backupWorker);
                    update(GenerateRoutineStatus(routine), false);
                }
            );
        }

        private void StartRestore(BackupRoutine routine, Action<string, bool> update)
        {
            RestoreWorker restoreWorker = new RestoreWorker(routine);
            _runningWorkerInstances.Add(restoreWorker);

            restoreWorker.Work(
                onProgress: (p, m) => update(p < 0 ? m : "Restoring: " + (int)Math.Round((double)p / restoreWorker.GetItemCount() * 100) + "%", true),
                onError: (e) => MessageBox.Show($"Restoring of {routine.Name} failed ({e.Message})"),
                onSuccess: () => MessageBox.Show($"Restoring of {routine.Name} succeeded"),
                onFinally: () =>
                {
                    _runningWorkerInstances.Remove(restoreWorker);
                    update(GenerateRoutineStatus(routine), false);
                }
            );
        }

        private void OpenDestinations(BackupRoutine routine)
        {
            // Clone and reverse list
            var reversed = routine.DestinationPaths.ToList().Reverse<string>().ToList();
            reversed.ForEach((d) => Process.Start("explorer.exe", "\"" + d + "\""));
        }

        private void OpenSettings(BackupRoutine routine)
        {
            new RoutineSettingsWindow(
                routine,
                onDelete: () =>
                {
                    _config.Routines.Remove(routine);
                    WriteConfiguration();
                    ReloadInnerGrid();
                }
            ).ShowDialog();

            WriteConfiguration();
            ReloadInnerGrid();
        }

        private void CreateAndShowShortcut(BackupRoutine routine)
        {
            string appLocation = Assembly.GetExecutingAssembly().Location;
            string linkPath = Path.Combine(Path.GetDirectoryName(appLocation), routine.Name + ".lnk");
            string iconLocation = ImageHelper.GenerateBackupIcon(routine);

            WshShell wsh = new WshShell();
            IWshShortcut shortcut = wsh.CreateShortcut(linkPath) as IWshShortcut;
            shortcut.Arguments = $"/start {routine.Name}";
            shortcut.TargetPath = appLocation;
            shortcut.WindowStyle = 1;
            shortcut.Description = $"Start backupping of {routine.Name} in ArchiveBakup";
            shortcut.WorkingDirectory = Path.GetDirectoryName(appLocation);
            shortcut.IconLocation = iconLocation;
            shortcut.Save();

            // Show shortcut in explorer
            Process.Start("explorer.exe", $"/select, \"{linkPath}\"");
        }

        private string GenerateRoutineStatus(BackupRoutine routine)
        {
            var latestArchiveFile = ArchiveFile.GetLatestArchiveFile(routine);

            return latestArchiveFile == null ? "No backup found!" : "Latest backup: " + latestArchiveFile.DateTime.ToString(FormattingConstants.DISPLAY_DATE_TIME_FORMAT);
        }
    }
}