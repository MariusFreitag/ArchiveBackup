﻿using ArchiveBackup.Configuration;
using ArchiveBackup.Helper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ArchiveBackup
{
    public partial class RoutineSettingsWindow : Window
    {
        private BackupRoutine _routine;
        private Action _onDelete;

        public RoutineSettingsWindow(BackupRoutine routine, Action onDelete)
        {
            InitializeComponent();

            DataContext = routine;

            _routine = routine;
            _onDelete = onDelete;
        }

        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {
            var txt = ((TextBox)sender);
            var lastIndex = txt.CaretIndex;

            for (int i = 0; i < _routine.Name.Length; i++)
            {
                if (!new Regex(FormattingConstants.NAME_REGEX).IsMatch(_routine.Name[i].ToString()))
                    _routine.Name = _routine.Name.Remove(i, 1);
            }

            if (string.IsNullOrEmpty(_routine.Name))
            {
                _routine.Name = "_";
                lastIndex = 1;
            }

            txt.CaretIndex = lastIndex;
            this.Title = _routine.Name + " Settings";
        }

        private void Delete_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (MessageBox.Show("Do you really want to delete this backup routine?", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                this.Close();
                _onDelete();
            }
        }

        private void FileFilter_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://github.com/icsharpcode/SharpZipLib/wiki/FastZip");
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}