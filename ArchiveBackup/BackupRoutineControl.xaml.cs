﻿using ArchiveBackup.Configuration;
using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace ArchiveBackup
{
    public partial class BackupRoutineControl : UserControl
    {
        private BackupRoutine _routine;
        private Action<Action<string, bool>> _onBackup;
        private Action<Action<string, bool>> _onRestore;
        private Action _onOpen;
        private Action _onSettings;
        private Action _onCreateShortcut;

        public BackupRoutineControl(BackupRoutine routine, Action<Action<string, bool>> onBackup, Action<Action<string, bool>> onRestore, Action onOpen, Action onSettings, Action onCreateShortcut)
        {
            InitializeComponent();

            DataContext = routine;

            _routine = routine;
            _onBackup = onBackup;
            _onRestore = onRestore;
            _onOpen = onOpen;
            _onSettings = onSettings;
            _onCreateShortcut = onCreateShortcut;
        }

        private void Backup_Click(object sender, RoutedEventArgs e) => _onBackup(this.SetStatus);

        private void Restore_Click(object sender, RoutedEventArgs e) => _onRestore(this.SetStatus);

        private void Open_Click(object sender, RoutedEventArgs e) => _onOpen();

        private void Settings_Click(object sender, RoutedEventArgs e) => _onSettings();

        private void Picture_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) => _onCreateShortcut();

        public void SetStatus(string status, bool active)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    StatusLabel.Content = status;
                    StatusLabel.Foreground = active ? Brushes.Green : Brushes.Black;
                    StatusLabel.FontWeight = active ? FontWeights.Bold : FontWeights.Regular;
                    BackupButton.IsEnabled = RestoreButton.IsEnabled = SettingsButton.IsEnabled = !active;
                    RestoreButton.IsEnabled = RestoreButton.IsEnabled && _routine.IsRestorable;
                });
            }
            catch
            {
            }
        }
    }
}