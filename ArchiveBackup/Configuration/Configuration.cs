﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveBackup.Configuration
{
    public class Configuration
    {
        public List<BackupRoutine> Routines { get; set; } = new List<BackupRoutine>();
    }
}
