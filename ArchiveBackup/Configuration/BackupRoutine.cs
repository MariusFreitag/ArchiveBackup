﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Data;
using System.ComponentModel;

namespace ArchiveBackup.Configuration
{
    public class BackupRoutine : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // For notifying changes
        [NonSerialized]
        private string _name = "";
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }

        public string PicturePath { get; set; } = "";

        public bool IsRestorable { get; set; } = false;

        public int BackupCount { get; set; } = 0;

        public string FileFilter { get; set; } = "";

        public List<string> SourcePaths { get; set; } = new List<string>();

        public List<string> DestinationPaths { get; set; } = new List<string>();

        public BackupRoutine(String name)
        {
            Name = name;
        }
    }
}
