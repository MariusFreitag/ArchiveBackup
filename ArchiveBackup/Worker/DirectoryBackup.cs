﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveBackup.Worker
{
    class DirectoryBackup
    {
        public string LocalFolder { get; set; }
        public string TargetDirectory { get; set; }

        public DirectoryBackup(string localFolder, string targetDirectory)
        {
            LocalFolder = localFolder;
            TargetDirectory = targetDirectory;
        }
    }
}
