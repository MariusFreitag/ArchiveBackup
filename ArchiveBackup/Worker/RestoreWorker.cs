﻿using ArchiveBackup.Configuration;
using ArchiveBackup.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ArchiveBackup.Worker
{
    class RestoreWorker : IWorker
    {
        private BackupRoutine _routine;
        private int _itemCount = -1;
        private int _restoreProgress = 0;
        private Thread _thread;

        private ArchiveFile _latestArchiveFile;

        public RestoreWorker(BackupRoutine routine)
        {
            _routine = routine;
        }

        public void Work(Action<int, string> onProgress, Action<Exception> onError, Action onSuccess, Action onFinally)
        {
            _thread = new Thread(() =>
            {
                string tempFolder = null;
                try
                {
                    onProgress(-1, "Initializing restoring..");
                    _latestArchiveFile = ArchiveFile.GetLatestArchiveFile(_routine);
                    tempFolder = Path.Combine(Path.GetTempPath(), "ArchiveBackup-" + _routine.Name + "-" + CryptographyHelper.CalculateRandomizedHash(_routine.SourcePaths.First()));
                    DirectoryHelper.CreateAsDir(tempFolder, true);

                    ExtractArchive(tempFolder, onProgress);

                    onProgress(-1, "Restoring directories..");
                    RestoreDirectories(tempFolder);

                    onSuccess();
                }
                catch (Exception e)
                {
                    LogHelper.AddError($"Restoring {_routine.Name}: " + e.ToString());
                    onError(e);
                }
                finally
                {
                    DirectoryHelper.DeleteIfExists(tempFolder);

                    onFinally();
                }
            }
            );
            _thread.Start();
        }

        private void ExtractArchive(string tempFolder, Action<int, string> onProgress)
        {
            _latestArchiveFile.Extract(tempFolder,
            (sender, args) =>
            {
                _restoreProgress++;
                onProgress(_restoreProgress, "Extracting " + Path.GetFileName(args.Name));
            }
            );
        }

        private void RestoreDirectories(string tempFolder)
        {
            RestoreMetadata metadata = JsonConvert.DeserializeObject<RestoreMetadata>(File.ReadAllText(Path.Combine(tempFolder, "metadata.json")));

            foreach (DirectoryBackup dir in metadata.DirectoryBackups)
            {
                DirectoryHelper.CreateAsDir(dir.TargetDirectory, true);

                if (dir.LocalFolder != null)
                {
                    DirectoryHelper.Copy(Path.Combine(tempFolder, dir.LocalFolder), dir.TargetDirectory);
                }
            }
        }

        public void Abort()
        {
            _thread.Abort();
        }

        public int GetItemCount()
        {
            if (_itemCount >= 0)
            {
                return _itemCount;
            }

            _itemCount = _latestArchiveFile.ItemCount;

            return _itemCount;
        }
    }
}
