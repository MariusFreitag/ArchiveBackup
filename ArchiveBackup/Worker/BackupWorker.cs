﻿using ArchiveBackup.Configuration;
using ArchiveBackup.Helper;
using ICSharpCode.SharpZipLib.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ArchiveBackup.Worker
{
    class BackupWorker : IWorker
    {
        private BackupRoutine _routine;
        private int _itemCount = -1;
        private int _backupProgress = 0;
        private List<String> _backupFiles = new List<string>();
        private Thread _thread;

        public BackupWorker(BackupRoutine routine)
        {
            _routine = routine;
        }

        public void Work(Action<int, String> onProgress, Action<Exception> onError, Action onSuccess, Action onFinally)
        {
            _thread = new Thread(() =>
            {
                string mergedSourceDirectory = null;

                try
                {
                    if (_routine.SourcePaths.All(string.IsNullOrWhiteSpace) || _routine.DestinationPaths.All(string.IsNullOrWhiteSpace))
                    {
                        MessageBox.Show($"{_routine.Name} must have at least one source and one destination directory!");
                        return;
                    }

                    onProgress(-1, "Initializing backup..");
                    string archiveName = Path.Combine(_routine.DestinationPaths[0], $"{_routine.Name} {DateTime.Now.ToString(FormattingConstants.FILE_DATE_TIME_FORMAT)}.zip");
                    mergedSourceDirectory = MergeDirectories();

                    PerformBackup(archiveName, mergedSourceDirectory, onProgress);
                    DuplicateArchiveFile(archiveName, onProgress);

                    onSuccess();
                }
                catch (Exception e1)
                {
                    LogHelper.AddError($"Backupping {_routine.Name}: " + e1.ToString());

                    foreach (var backupFile in _backupFiles)
                    {
                        try
                        {
                            File.Delete(backupFile);
                            LogHelper.AddInfo("The failed archive \"" + backupFile + "\" was deleted successfully.");
                        }
                        catch (Exception e2)
                        {
                            LogHelper.AddInfo("The failed archive \"" + backupFile + "\" was NOT deleted successfully: " + e2);
                        }
                    }

                    onError(e1);
                }
                finally
                {
                    // Delete temp folder when used
                    if (_routine.SourcePaths.Count > 1 && mergedSourceDirectory != null)
                    {
                        DirectoryHelper.DeleteIfExists(mergedSourceDirectory);
                    }

                    onFinally();
                }
            }
            );
            _thread.Start();
        }

        private string MergeDirectories()
        {
            // Datastructure to store restoring metadata
            // Format: source -> folder
            RestoreMetadata metadata = new RestoreMetadata();

            // If there is only one source and the routine is not restorable,
            // the files do not have to be copied to a temp folder
            if (!_routine.IsRestorable && _routine.SourcePaths.Count == 1)
            {
                // Check if the first source directory is a file
                if (File.Exists(_routine.SourcePaths.First()))
                {
                    throw new Exception("Source directory " + _routine.SourcePaths.First() + " is a file!");
                }

                return _routine.SourcePaths.First();
            }

            string mergedSourceDirectory = Path.Combine(Path.GetTempPath(), "ArchiveBackup-" + _routine.Name + "-" + CryptographyHelper.CalculateRandomizedHash(_routine.SourcePaths.First()));

            DirectoryHelper.CreateAsDir(mergedSourceDirectory, true);

            foreach (var sourcePath in _routine.SourcePaths.Where((p) => !string.IsNullOrEmpty(p)))
            {
                string dirName = Path.GetFileName(sourcePath) + "-" + CryptographyHelper.CalculateMD5Hash(sourcePath);

                // Check if the source directory is a file
                if (File.Exists(sourcePath))
                {
                    throw new Exception("Source directory " + sourcePath + " is a file!");
                }
                // Check if the source directory exists
                else if (Directory.Exists(sourcePath))
                {
                    metadata.AddDirectory(dirName, sourcePath);
                    DirectoryHelper.Copy(sourcePath, Path.Combine(mergedSourceDirectory, dirName));
                }
                else
                {
                    metadata.AddDirectory(null, sourcePath);
                }
            }

            // Write the restore metadata into a file if wanted
            if (_routine.IsRestorable)
            {
                File.WriteAllText(Path.Combine(mergedSourceDirectory, "metadata.json"), JsonConvert.SerializeObject(metadata, Formatting.Indented));
            }

            return mergedSourceDirectory;
        }

        private void PerformBackup(string archiveName, string sourceDirectory, Action<int, string> onProgress)
        {
            _backupFiles.Add(archiveName);

            ArchiveFile.CreateArchive(
                sourceDirectory,
                archiveName,
                _routine.FileFilter,
                (sender, args) =>
                {
                    _backupProgress++;
                    onProgress(_backupProgress, "Adding " + Path.GetFileName(args.Name));
                }
            );
        }

        private void DuplicateArchiveFile(string archiveName, Action<int, string> onProgress)
        {
            if (_routine.DestinationPaths.Count > 1)
            {
                onProgress(-1, "Duplicating Archive..");

                for (int i = 1; i < _routine.DestinationPaths.Count; i++)
                {
                    DirectoryHelper.CreateAsDir(_routine.DestinationPaths[i]);

                    string duplicatedArchiveName = Path.Combine(_routine.DestinationPaths[i], Path.GetFileName(archiveName));

                    _backupFiles.Add(Path.Combine(_routine.DestinationPaths[i], Path.GetFileName(archiveName)));

                    File.Copy(archiveName, duplicatedArchiveName);
                }
            }
        }

        public void Abort()
        {
            _thread.Abort();
        }

        public int GetItemCount()
        {
            if (_itemCount >= 0)
            {
                return _itemCount;
            }

            _itemCount = 0;

            foreach (var sourcePath in _routine.SourcePaths)
            {
                if (Directory.Exists(sourcePath))
                {
                    _itemCount += Directory.GetFiles(sourcePath, "*", SearchOption.AllDirectories).Length;
                }
                else
                {
                    _itemCount += 1;
                }
            }

            if (_routine.IsRestorable)
            {
                _itemCount++;
            }

            return _itemCount;
        }
    }
}
