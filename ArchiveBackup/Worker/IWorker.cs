﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveBackup.Worker
{
    interface IWorker
    {
        void Work(Action<int, string> onProgress, Action<Exception> onError, Action onSuccess, Action onFinally);
        void Abort();
        int GetItemCount();
    }
}
