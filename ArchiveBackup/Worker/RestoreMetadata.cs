﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveBackup.Worker
{
    class RestoreMetadata
    {
        public List<DirectoryBackup> DirectoryBackups { get; set; } = new List<DirectoryBackup>();

        public void AddDirectory(string localFolder, string targetDirectory)
        {
            DirectoryBackups.Add(new DirectoryBackup(localFolder, targetDirectory));
        }
    }
}
