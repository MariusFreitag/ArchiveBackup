﻿using Microsoft.WindowsAPICodePack.Taskbar;
using System;
using System.Threading;
using System.Windows;

namespace ArchiveBackup
{
    public partial class ProgressWindow : Window
    {
        private TaskbarManager _taskbarManager;
        private Action _onCancel;
        private bool _isClosing = false;

        public ProgressWindow(String title, Action onCancel)
        {
            InitializeComponent();

            _taskbarManager = TaskbarManager.Instance;

            Title = title + " is backupping..";
            ProgressBar.Maximum = 100;
            ProgressLabel.Content = "";
            _onCancel = onCancel;
        }

        public new void Close()
        {
            _isClosing = true;
            this.Close();
        }

        public void SetProgress(int maxProgress, int progress, string currentFile)
        {
            _taskbarManager.SetProgressState(TaskbarProgressBarState.Normal);
            _taskbarManager.SetProgressValue(progress, maxProgress);

            Dispatcher.Invoke(delegate
            {
                ProgressLabel.Content = currentFile;
                ProgressBar.Maximum = maxProgress;
                ProgressBar.Value = progress;
            });
        }

        public void SetIndeterminate(bool indeterminate, string message)
        {
            _taskbarManager.SetProgressState(indeterminate ? TaskbarProgressBarState.Indeterminate : TaskbarProgressBarState.Normal);

            Dispatcher.Invoke(delegate
            {
                ProgressBar.IsIndeterminate = indeterminate;
                ProgressLabel.Content = message;
            });
        }

        public void ShowMessageBox(string message, string caption = "")
        {
            Dispatcher.Invoke(() => MessageBox.Show(this, message, caption));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_isClosing)
            {
                _onCancel();
                e.Cancel = true;
            }
        }
    }
}