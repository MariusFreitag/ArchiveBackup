﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;

namespace ArchiveBackup.Helper
{
    static class DependencyDownloader
    {
        public static void Download(Dictionary<String, String> dependencies)
        {
            try
            {
                foreach (var dependency in dependencies)
                {
                    try
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(dependency.Key)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(dependency.Value));
                        }
                        if (!File.Exists(dependency.Value))
                        {
                            new WebClient().DownloadFile(dependency.Key, dependency.Value);
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(
                            $"Missing dependency: ({Path.GetFileName(dependency.Value)}).{Environment.NewLine}Please establish a network connextion and try again.");
                        Environment.Exit(0);
                    }
                }

                AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
                    (
                        from dependency in dependencies
                        where dependency.Value.Contains(
                            args.Name.Substring(0, args.Name.IndexOf(",", StringComparison.Ordinal)))
                        select Assembly.LoadFrom(dependency.Value)
                    ).FirstOrDefault();
            }
            catch
            {
                MessageBox.Show(
                    "The initialization process of the program failed. Please close all instances and try to establish a network connection.");
                Environment.Exit(0);
            }
        }
    }
}