﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveBackup.Helper
{
    static class FormattingConstants
    {
        public static readonly string
            NAME_REGEX = "[A-Za-z0-9_-]+",
            FILE_NAME_REGEX = "(" + NAME_REGEX + ") [0-9]{2}-[0-9]{2}-[0-9]{4} [0-9]{2}-[0-9]{2}-[0-9]{2}.zip",
            FILE_DATE_TIME_FORMAT = "dd-MM-yyyy HH-mm-ss",
            DISPLAY_DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
    }
}
