﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchiveBackup.Helper
{
    static class LogHelper
    {
        private static readonly string Logpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MariusFreitag", "ArchiveBackup", "archivebackup_error.log");

        private static readonly string[] Header =
        {
            "###############################################################",
            "Log automatically created by ArchiveBackup (do not modify!)",
            "###############################################################"
        };

        static LogHelper()
        {
            if (File.Exists(Logpath))
            {
                string[] logContent = File.ReadAllLines(Logpath);

                if (logContent.Length >= 4
                    && logContent[0] == Header[0] && logContent[1] == Header[1] && logContent[2] == Header[2])
                {
                    return;
                }
            }

            Clear();
        }

        public static void AddError(string message)
        {
            AddCustom("ERROR", message);
        }

        public static void AddInfo(string message)
        {
            AddCustom("INFO", message);
        }

        public static void AddCustom(string messagetype, string message)
        {
            File.AppendAllText(Logpath,
                Environment.NewLine + Environment.NewLine + "ArchiveBackup " + messagetype + " [" +
                DateTime.Now.ToString(FormattingConstants.DISPLAY_DATE_TIME_FORMAT) + "]: " + Environment.NewLine +
                message);
        }

        public static void Clear()
        {
            DirectoryHelper.DeleteIfExists(Logpath);
            File.WriteAllLines(Logpath, Header);

            AddInfo("Log created");
        }
    }
}