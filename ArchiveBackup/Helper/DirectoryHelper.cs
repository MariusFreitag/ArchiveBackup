﻿using System.IO;

namespace ArchiveBackup.Helper
{
    static class DirectoryHelper
    {
        public static void CreateAsDir(string dir, bool deleteIfExists = false)
        {
            if (File.Exists(dir))
            {
                File.Delete(dir);
            }
            if (deleteIfExists)
            {
                DeleteIfExists(dir);
            }
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public static void Copy(string source, string destination)
        {
            if (!Directory.Exists(source))
                return;

            CreateAsDir(destination, true);

            // Create all new directories
            foreach (string dirPath in Directory.GetDirectories(source, "*", SearchOption.AllDirectories))
            {
                CreateAsDir(dirPath.Replace(source, destination), true);
            }

            // Copy all files
            foreach (string newPath in Directory.GetFiles(source, "*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(source, destination), true);
            }
        }

        public static void DeleteIfExists(string dir)
        {
            if (!Directory.Exists(dir))
                return;

            SetAttributesNormal(new DirectoryInfo(dir));
            Directory.Delete(dir, true);
        }

        private static void SetAttributesNormal(DirectoryInfo dir)
        {
            foreach (var subDir in dir.GetDirectories())
            {
                SetAttributesNormal(subDir);
            }

            foreach (var file in dir.GetFiles())
            {
                file.Attributes = FileAttributes.Normal;
            }
        }
    }
}