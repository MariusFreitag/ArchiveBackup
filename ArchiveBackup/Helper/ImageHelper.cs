﻿using ArchiveBackup.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.IconLib;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Image = System.Drawing.Image;
using Point = System.Drawing.Point;
using Rectangle = System.Drawing.Rectangle;

namespace ArchiveBackup.Helper
{
    static class ImageHelper
    {
        public static ImageSource BitmapToImageSource(Bitmap bitmap)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(bitmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }

        public static Bitmap ReadBitmap(string path)
        {
            List<Func<Bitmap>> methods = new List<Func<Bitmap>>()
                {
                    () => new Icon(path, new System.Drawing.Size(256, 256)).ToBitmap(),
                    () => new Bitmap(path),
                    () => Icon.ExtractAssociatedIcon(path).ToBitmap()
                };

            foreach (var method in methods)
            {
                try
                {
                    return method();
                }
                catch
                {

                }
            }

            return CreateDummyBitmap(256, 256, System.Drawing.Brushes.White);
        }

        public static Bitmap CreateDummyBitmap(int width, int height, System.Drawing.Brush brush)
        {
            Bitmap dummy = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (var graphics = Graphics.FromImage(dummy))
            {
                graphics.FillRectangle(brush, new Rectangle(0, 0, width, height));
            }

            return dummy;
        }

        public static string GenerateBackupIcon(BackupRoutine routine, string path = null)
        {
            path = path ?? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MariusFreitag", "ArchiveBackup", "img", routine.Name + ".ico");

            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }

            var mergedIcon = MergeIcons(ResizeImage(ReadBitmap(routine.PicturePath), 256, 256), ReadBitmap(System.Reflection.Assembly.GetEntryAssembly().Location));

            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                MultiIcon mIcon = new MultiIcon();
                mIcon.Add("icon").CreateFrom((Bitmap)mergedIcon);
                mIcon.SelectedIndex = 0;
                mIcon.Save(fs, MultiIconFormat.ICO);
            }

            return path;
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        private static Image MergeIcons(Image mainImage, Image indexImage)
        {
            // Create Bitmap with size of mainImage
            Bitmap outputImage = new Bitmap(mainImage.Width, mainImage.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            var indexSize = new System.Drawing.Size(mainImage.Width / 2, mainImage.Height / 2);

            using (Graphics graphics = Graphics.FromImage(outputImage))
            {
                // Draw mainImage
                graphics.DrawImage(mainImage, new Rectangle(new Point(), mainImage.Size),
                    new Rectangle(new Point(), mainImage.Size), GraphicsUnit.Pixel);

                // Draw indexImage small in the lower right corner
                graphics.DrawImage(indexImage, new Rectangle(new Point(mainImage.Width - indexSize.Width, mainImage.Height - indexSize.Height), indexSize),
                    new Rectangle(new Point(), indexImage.Size), GraphicsUnit.Pixel);
            }

            return outputImage;
        }
    }
}
