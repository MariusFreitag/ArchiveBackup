﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Core;
using System.Text.RegularExpressions;
using ArchiveBackup.Configuration;

namespace ArchiveBackup.Helper
{
    class ArchiveFile
    {
        public string RoutineName { get; set; }
        public string FilePath { get; set; }
        public DateTime DateTime { get; set; }

        public int ItemCount
        {
            get
            {
                try
                {
                    return (int)new ZipFile(FilePath).Count;
                }
                catch
                {
                    return 0;
                }
            }
        }

        private ArchiveFile(string backupName, string filePath, DateTime dateTime)
        {
            this.RoutineName = backupName;
            this.FilePath = filePath;
            this.DateTime = dateTime;
        }

        public void Extract(string targetDirectory, ProcessFileHandler onProcess)
        {
            DirectoryHelper.CreateAsDir(targetDirectory, true);

            new FastZip(new FastZipEvents { ProcessFile = onProcess }) { CreateEmptyDirectories = true }.ExtractZip(FilePath, targetDirectory, null);
        }

        public void Delete()
        {
            int tryCount = 50;
            while (tryCount > 0)
            {
                try
                {
                    File.Delete(FilePath);
                    tryCount = 0;
                }
                catch
                {
                    tryCount--;
                }
            }
        }

        #region Static methods

        public static ArchiveFile CreateArchive(string source, string destination, string fileFilter, ProcessFileHandler onProcess)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new Exception("Source directory name empty");
            }

            if (!Directory.Exists(Path.GetDirectoryName(destination)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(destination));
            }

            new FastZip(new FastZipEvents { ProcessFile = onProcess }) { CreateEmptyDirectories = true }.CreateZip(destination, source, true, fileFilter);

            return ReadArchive(destination);
        }

        public static ArchiveFile ReadArchive(string filePath)
        {
            string[] info = Path.GetFileNameWithoutExtension(filePath).Split(' ');

            return new ArchiveFile(info[0],
                filePath,
                DateTime.ParseExact(info[1] + " " + info[2], FormattingConstants.FILE_DATE_TIME_FORMAT,
                    System.Globalization.CultureInfo.InvariantCulture)
            );
        }

        public static ArchiveFile GetLatestArchiveFile(BackupRoutine routine)
        {
            // Abort when no destination paths are defined
            if (routine.DestinationPaths.Count <= 0)
            {
                return null;
            }

            return ReadAllArchivesFiles(routine.DestinationPaths[0]).Where((a) => a.RoutineName == routine.Name).OrderByDescending((a) => a.DateTime).FirstOrDefault();
        }

        public static void CleanupArchiveFiles(BackupRoutine routine)
        {
            // Abort when no destination paths are defined or not existing
            if (routine.DestinationPaths.Count <= 0)
            {
                return;
            }

            // Delete old archive files in first destination path
            while (routine.BackupCount > 0 &&
                ReadAllArchivesFiles(routine.DestinationPaths.First()).Where((a) => a.RoutineName == routine.Name)
                    .Count() > routine.BackupCount)
            {
                var oldest = ReadAllArchivesFiles(routine.DestinationPaths[0]).Where((a) => a.RoutineName == routine.Name).OrderBy((a) => a.DateTime).FirstOrDefault();
                oldest.Delete();
            }
        }

        private static List<ArchiveFile> ReadAllArchivesFiles(string directory)
        {
            if (!Directory.Exists(directory))
            {
                return new List<ArchiveFile>();
            }

            List<ArchiveFile> archiveFiles = new List<ArchiveFile>();

            foreach (var file in Directory.GetFiles(directory).Where(file => new Regex(FormattingConstants.FILE_NAME_REGEX).IsMatch(file)))
            {
                archiveFiles.Add(ReadArchive(file));
            }

            return archiveFiles;
        }

        #endregion
    }
}
