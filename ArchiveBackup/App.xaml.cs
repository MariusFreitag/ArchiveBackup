﻿using ArchiveBackup.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace ArchiveBackup
{
    public partial class App : Application
    {
        protected void App_Startup(object sender, StartupEventArgs e)
        {
            string dependencyRootUrl = "https://mariusfreitag.de/lib/";
            string dependencyPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MariusFreitag", "ArchiveBackup", "lib");
            DependencyDownloader.Download(new Dictionary<string, string>
                {
                    { dependencyRootUrl + "IconLib.dll", Path.Combine(dependencyPath, "IconLib.dll") },
                    { dependencyRootUrl + "Newtonsoft.Json.dll", Path.Combine(dependencyPath, "Newtonsoft.Json.dll") },
                    { dependencyRootUrl + "ICSharpCode.SharpZipLib.dll", Path.Combine(dependencyPath, "ICSharpCode.SharpZipLib.dll") },
                    { dependencyRootUrl + "Microsoft.WindowsAPICodePack.dll", Path.Combine(dependencyPath, "Microsoft.WindowsAPICodePack.dll") },
                    { dependencyRootUrl + "Microsoft.WindowsAPICodePack.Shell.dll", Path.Combine(dependencyPath, "Microsoft.WindowsAPICodePack.Shell.dll") },
                    { dependencyRootUrl + "Microsoft.WindowsAPICodePack.ShellExtensions.dll", Path.Combine(dependencyPath, "Microsoft.WindowsAPICodePack.ShellExtensions.dll") }
                }
            );

            new MainWindow().Show();
        }
    }
}